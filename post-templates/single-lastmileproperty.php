<?php
/**
 * Template for single Last Mile Property post view
 */
?>

<?php get_header(); ?>

<?php if( have_posts() ) while ( have_posts() ) : the_post(); ?>
<script>
	//DISTANCE
	var overview_vars = {
		distance_defaut_tab: "<?php echo papi_get_field('distance_values')[0]['distance_value_slug']; ?>",
		distance_default: <?php echo papi_get_field('distance_values')[0]['distance_value_number']; ?>,
		time_defaut_tab: "<?php echo papi_get_field('time_values')[0]['time_value_slug']; ?>",
		time_default: <?php echo papi_get_field('time_values')[0]['time_value_number']; ?>
	}
	//MAP JSON
	var map_vars = {
		zoom: <?php the_papi_field('map_zoom'); ?>,
		pin: '<?php echo $HPLM_PATH ?>assets/images/mapmarker.png',
		center: {lat: <?php the_papi_field('map_center_lat'); ?>, lng: <?php the_papi_field('map_center_long'); ?>},
		markers: [
		<?php foreach(papi_get_field('map_markers') as $key => $marker ):
			echo "{label: '".$marker['map_marker_label']."',text: '".$marker['map_marker_text']."',lat:".$marker['map_marker_latitude'].",lng:".$marker['map_marker_longitude']."}";
			echo ($marker === end( papi_get_field('map_markers') )) ? "" : ","; 
			echo "\n\t\t";
		endforeach; ?>
		]
	};
	//CALLBACK
	function hplm_mapping(){
		console.log('hello');
		HPLM.mapping.init();
	};
</script>
<div class="hplm-parent">
	<!--BEGIN HPLM HEADER-->
	<header class="hplm-header" id="hplm-header">
			<div id="hplm-header-image" class="hplm-header-image"></div>
			<div class="hplm-section hplm-intro hplm-background--hashed">
				<div class="hplm-ribbon">
					<h1 class="hplm-typecolor--orange hplm-typesize--16 hplm-typeweight--bold hplm-typespace--sm"><?php the_papi_field("label_title"); ?></h1>
				</div>
				<div class="hplm-intro-welcome" id="hplm-intro-welcome">
					<h2 class="hplm-typesize--90 hplm-typecolor--orange hplm-typealign--center hplm-typestyle--dropshadow hplm-typeweight--bold"><?php the_papi_field("intro_title"); ?></h2>
					<p class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--orange hplm-typealign--center"><?php the_papi_field("intro_text"); ?></p>
				</div>
			</div>
			<div class="hplm-download">
				<a href="<?php echo papi_get_field('pdf_spec_sheet')->url; ?>" target="_blank">Download PDF</a>
			</div>
	</header>
	<!--END HPLM HEADER-->
	<!--BEGIN HPLM TABS-->
	<div class="hplm-tabs tabs">
		<nav id="hplm-nav" class="hplm-nav" role="tablist" aria-label="Entertainment">
			<button role="tab" aria-selected="true" aria-controls="overview-tab" id="overview" tabindex="-1">Overview</button>
			<button role="tab" aria-selected="false" aria-controls="about-tab" id="about" tabindex="-1" data-deletable="">About</button>
			<button role="tab" aria-selected="false" aria-controls="sitespecs-tab" id="sitespecs">Site Specs</button>
			<button role="tab" aria-selected="false" aria-controls="utilities-tab" id="utilities" tabindex="-1" data-deletable="">Utilities</button>
			
		</nav>
		<div class="hplm-tab"  tabindex="0" role="tabpanel" id="overview-tab" aria-labelledby="overview">
			<?php include('tab-overview.php'); ?>
		</div>
		<div class="hplm-tab" tabindex="0" role="tabpanel" id="utilities-tab" aria-labelledby="utilities" hidden="hidden">
			<?php include('tab-utilities.php'); ?>
		</div>
		<div class="hplm-tab" tabindex="0" role="tabpanel" id="about-tab" aria-labelledby="about" hidden="hidden">
			<?php include('tab-about.php'); ?>
		</div>
		<div class="hplm-tab" tabindex="0" role="tabpanel" id="sitespecs-tab" aria-labelledby="sitespecs" hidden="hidden">
			<?php include('tab-sitespecs.php'); ?>
		</div>
			
	<!--END HPLM TABS-->
	<footer class="hplm-footer hplm-background--hashed hplm-background--blueLight">
			<h2 class="hplm-typesize--42 hplm-typeweight--bold hplm-typecolor--orange hplm-typealign--center">Ready to go the last mile?</h2>
			<p class="hplm-typesize--18 hplm-typeweight--bold hplm-typecolor--orange hplm-typealign--center"><?php the_papi_field('cta_text'); ?></p>
			<p class="hplm-typealign--center"><a href="/contact" class="hplm-button hplm-button--lg">or Email Us</a></p>
	</footer>
</div>
<!--END HPLM PARRENT-->
<?php endwhile; ?>
<?php get_footer(); ?>