<h1 class="hplm-sr">Site Specs</h1>
<div class="hplm-image--fullwidth hplm-background--hashed">
	<img src="<?php echo papi_get_field('sitespecs_map')->url; ?>" />
</div>
<div class="hplm-table-container">
	<table class="hplm-data hplm-data-sitespecs hplm-typesize--16 hplm-typecolor--blue hplm-typeweight--bold" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
		        <td></td>
	            <td>Building</td>
	            <td>Const Date</td>
	            <td>Type</td>
	            <td>Total SF</td>
	            <td>WHSE<br/>Office SF</td>
	            <td>Column<br/>Clear</td>
	            <td>Doc<br/>Spacing</td>
	            <td>FRZR/<br/>Position</td>
	            <td>Temp<br/>Refrig</td>
	            <td>Range</td>
	            <td>Tenant</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php foreach(papi_get_field('sitespecs_values') as $key => $value): ?>
	        <tr>   
		        <td><?php echo ($key +1); ?></td>
				<td><?php echo $value['spec_building']; ?></td>
				<td><?php echo $value['spec_constdate']; ?></td>
				<td><?php echo $value['spec_type']; ?></td>
				<td><?php echo $value['spec_totalsf']; ?></td>
				<td><?php echo $value['spec_whseofficesf']; ?></td>
				<td><?php echo $value['spec_columnclear']; ?></td>
				<td><?php echo $value['spec_docspacing']; ?></td>
				<td><?php echo $value['spec_frzrposition']; ?></td>
				<td><?php echo $value['spec_temprefrig']; ?></td>
				<td><?php echo $value['spec_range']; ?></td>
				<td class="<?php echo $value['spec_tenant']; ?>"><?php echo $value['spec_tenant']; ?></td>
	        </tr>    
	        <?php endforeach; ?>
	    </tbody>
	    <tfoot>
		    <?php foreach(papi_get_field('sitespecs_totals') as $key => $value): ?>
		    <tr>
		        <td></td>
	            <td>&nbsp;</td>
	            <td>&nbsp;</td>
	            <td>&nbsp;</td>
	            <td><?php echo $value['spec_totals_totalsf']; ?></td>
				<td><?php echo $value['spec_totals_whseofficesf']; ?></td>
				<td><?php echo $value['spec_totals_columnclear']; ?></td>
	            <td>&nbsp;</td>
	            <td><?php echo $value['spec_totals_frzrposition']; ?></td>
	            <td>&nbsp;</td>
	            <td>&nbsp;</td>
	            <td>&nbsp;</td>
	        </tr>
	        <?php endforeach; ?>
	    </tfoot>
	</table>
</div>