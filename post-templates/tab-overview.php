<!--------------------------------------------------------------------------------------------
SECTION: DISTANCE
-------------------------------------------------------------------------------------------->
<article class="hplm-section hplm-distance" id="section-1">
        <h2 class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--blue hplm-typealign--center">The Alameda Warehouse is the best <br class="hplm-hide-lg"/>location for reaching online shoppers.</h2>
        <div class="hplm-distance-graphic">
                <p class="hplm-distance-percentage hplm-typealign--center hplm-typecolor--orange"><span id="hplm-distance-percentage">0</span> %</p>
                <div class="hplm-silhouettes">
                </div>
        </div>
        <h3 class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--blue hplm-typealign--center">Online Shoppers within a 10 mile radius of:</h3>
        <div class="hplm-controls hplm-controls--distance">
	        <?php foreach( papi_get_field('distance_values') as $value): ?>
            <div class="hplm-button-wrapper"><button aria-selected="false" aria-controls="<?php echo $value['distance_value_slug']; ?>-tab" data-value="<?php echo $value['distance_value_number']; ?>" id="<?php echo $value['distance_value_slug']; ?>" tabindex="-1"><?php echo $value['distance_value_title']; ?></button><small><?php echo $value['distance_value_streets']; ?></small></div>
            <?php endforeach; ?>
        </div>
        <p class="hplm-typecolor--blue hplm-typesize--12 hplm-typeweight--bold hplm-typealign--center hplm-typepadding-top--md"><?php the_papi_field('distance_source'); ?></p>
</article>
<!--------------------------------------------------------------------------------------------
SECTION: MAP
-------------------------------------------------------------------------------------------->
<article class="hplm-section hplm-mapping" id="section-2">
    <div id="hplm-map" class="hplm-mapping-googlemap"></div>
</article>
<!--------------------------------------------------------------------------------------------
SECTION: TIME
-------------------------------------------------------------------------------------------->
<article class="hplm-section hplm-time" id="section-3">
    <h2 class="hplm-typesize--28 hplm-typecolor--orange hplm-typealign--center"><?php the_papi_field('time_section_title'); ?></h2>
    <div class="hplm-time-graphic hplm-sectionbreak">
            <div class="hplm-col--1 hplm-show-1024up">
                    <div class="hplm-graphic"></div>
                    <p class="hplm-typealign--center">&nbsp;<img src="<?php echo $HPLM_PATH ?>assets/images/hands.svg" class="hplm-graphic-inline hplm-graphic-inline--super"/>&nbsp;</p>
                    <p class="hplm-typealign--right hplm-typecolor--tealDark">Deliver more in a day.</p>
                    <p class="hplm-typealign--left hplm-typecolor--tealDark">Make happier customers. <img src="<?php echo $HPLM_PATH ?>assets/images/thumb.svg" class="hplm-graphic-inline hplm-graphic-inline--bottom"/></p>
            </div>
            <div class="hplm-col--2">
                <div class="hplm-clock">
                    <div class="hplm-clock-label hplm-typealign--center hplm-typecolor--orange hplm-typesize--28">Only</div>
                    <div class="hplm-clock-time  hplm-typealign--center hplm-typecolor--orange"><span id="hplm-clock-time">0</span>minutes</div>
                </div>
                <div class="hplm-controls hplm-controls--time">
                    <?php foreach( papi_get_field('time_values') as $value): ?>
			        	<button aria-selected="false" aria-controls="<?php echo $value['time_value_slug']; ?>-tab" id="<?php echo $value['time_value_slug']; ?>" data-value="<?php echo $value['time_value_number']; ?>" tabindex="-1"><?php echo $value['time_value_title']; ?></button>
		            <?php endforeach; ?>
                </div>
                <p class="hplm-typecolor--orange hplm-typesize--12 hplm-typeweight--bold hplm-typealign--center hplm-typepadding-top--md"><?php the_papi_field('time_source'); ?></p>
            </div>
            <div class="hplm-col--1 last hplm-show-1024up">
                    <p class="hplm-typealign--left hplm-typecolor--tealDark">Maximize resources&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo $HPLM_PATH ?>assets/images/fuel.svg" class="hplm-graphic-inline  hplm-graphic-inline--top"/></p>
                    <p class="hplm-typealign--right hplm-typecolor--tealDark">when you deliver faster.</p>
            </div>
    </div>
</article>
<!--------------------------------------------------------------------------------------------
SECTION: STATS
-------------------------------------------------------------------------------------------->
<article class="hplm-section hplm-stats hplm-background--hashed hplm-background--teal" id="section-4">
        <h2 class="hplm-sr">Some stats about the Alameda property.</h2>
        <div class="hplm-sectionbreak">
	        <div class="hplm-col-1of2">
	            <div class="hplm-stats-income">
	                <p class="hplm-typeweight--bold hplm-typecolor--blue hplm-typealign--left hplm-typesize--18">Households with an average income <br class="hplm-show-1024up"/>of $50K - $150K within a 10 mile radius.</p>
	                <p class="hplm-typeweight--bold hplm-typecolor--blue hplm-typealign--left stats-inline hplm-typemargin-bottom--xs"><span  class="hplm-typesize--18">Alameda</span> <span class="hplm-typesize--48">177,521</span> <br/>
	                        <span class="hplm-typesize--18">Buckeye</span> <span class="hplm-typesize--48">114,500</span> <br/>
	                        <span class="hplm-typesize--18">PV303</span> <span class="hplm-typesize--48">&nbsp;&nbsp;80,998</span></p>
					<p class="hplm-typecolor--blue hplm-typesize--12 hplm-typeweight--bold hplm-typealign--left">Source: CBRE 2018</p>
	            </div>
	            <div class="hplm-stats-shopping">
	                <div class="graphic hplm-typealign--right"><img src="<?php echo $HPLM_PATH ?>assets/images/chart.svg"></div>
	                <p class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--blue hplm-typealign--left">Alameda puts you closer to households with incomes <br class="hplm-show-1024up"/>that index the highest for <br class="hplm-show-1024up"/>online shopping.</p>
	            </div>
	        </div>
	        
	        <div class="hplm-col-1of2">
	            <div class="hplm-stats-college">
	                <p class="hplm-typesize--18 hplm-typeweight--bold hplm-typecolor--tealDark hplm-typealign--left">College educated shoppers <br class="hplm-hide-lg"/>within a 10 mile radius.</p>
	                <ul class="hplm-sectionbreak">
	                    <li class=" hplm-typeweight--bold hplm-typecolor--tealDark hplm-typealign--left">
	                        <span class="hplm-typesize--18">Alameda</span><br/>
	                        <span class="hplm-typesize--48">37%</span>
	                    </li>
	                    <li class="hplm-typeweight--bold hplm-typecolor--tealDark hplm-typealign--left">
	                            <span class="hplm-typesize--18">Buckeye</span><br/>
	                            <span class="hplm-typesize--48">20%</span>
	                    </li>
	                    <li class="hplm-typeweight--bold hplm-typecolor--tealDark hplm-typealign--left">
	                            <span class="hplm-typesize--18">PV303</span><br/>
	                            <span class="hplm-typesize--48">19%</span>
	                    </li>
	                </ul>
	                <p class="hplm-typecolor--tealDark hplm-typesize--12 hplm-typeweight--bold hplm-typealign--left hplm-typepadding-top--md">Source: CBRE 2018</p>
	                <div class="graphic hplm-typealign--right"><img src="<?php echo $HPLM_PATH ?>assets/images/grad.svg"></div>
	            </div>
	            <div class="hplm-stats-amazon">
	                <p class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--tealDark hplm-typealign--left hplm-typemargin-bottom--sm">Nearly half of all <br class="hplm-show-1024up"/>Amazon shoppers have <br class="hplm-hide-1024up"/>a college education.</p>
	                <p class="hplm-typecolor--tealDark hplm-typesize--12 hplm-typeweight--bold hplm-typealign--left">Source: Epsilon</p>
	                
	            </div>
	        </div>
        </div>
</article>
<!--------------------------------------------------------------------------------------------
SECTION: AGE
-------------------------------------------------------------------------------------------->
<article class="hplm-section hplm-age" id="section-5">
        <h2 class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--orange hplm-typealign--center"><?php the_papi_field('age_section_title'); ?></h2>
        <div class="hplm-age-graphic hplm-sectionbreak">
            <?php foreach( papi_get_field('age_values') as $value): ?>
            	<div class="hplm-col-1of3">
	                <div class="hplm-graph hplm-graph-<?php echo $value['age_value_slug']; ?>" id="hplm-graph-<?php echo $value['age_value_slug']; ?>">
	                    <div class="percent-outline"><?php echo $value['age_value_number']; ?>%</div>
	                    <div class="percent-solid"><?php echo $value['age_value_number']; ?>%</div>
	                    <div class="caption"><?php echo $value['age_value_title']; ?></div>
	                    <div class="pie"><div class="piece" data-value="<?php echo $value['age_value_number']; ?>"></div></div>
	                    <div class="graphic"><img src="<?php echo $HPLM_PATH ?>assets/images/smile-<?php echo $value['age_value_slug']; ?>.svg"></div>
	                </div>
	            </div>
            <?php endforeach; ?>
        </div>
        <p class="hplm-typesize--28 hplm-typeweight--bold hplm-typecolor--orange hplm-typealign--center"><?php the_papi_field('age_summary'); ?></p>
        <p class="hplm-typecolor--orange hplm-typesize--12 hplm-typeweight--bold hplm-typealign--center"><?php the_papi_field('age_source'); ?></p>
</article>
</div>