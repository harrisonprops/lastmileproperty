<h1 class="hplm-sr">About</h1>
<div class="hplm-table-container">
	<table class="hplm-data hplm-data-about hplm-typesize--16 hplm-typecolor--blue hplm-typeweight--bold" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
	            <td class="hplm-sr">Label</td>
	            <td class="hplm-sr">Value</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php foreach(papi_get_field('about_values') as $value): ?>
	        <tr>   
				<td><?php echo $value['about_label']; ?></td>
				<td><?php echo $value['about_value']; ?></td>
	        </tr>    
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>