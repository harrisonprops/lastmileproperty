<h1 class="hplm-sr">Utilities</h1>
<div class="hplm-table-container">
	<table class="hplm-data hplm-data-utilities hplm-typesize--16 hplm-typecolor--blue hplm-typeweight--bold" border="0" cellpadding="0" cellspacing="0">
	    <thead>
	        <tr>
	            <td>System</td>
	            <td>Description</td>
	        </tr>
	    </thead>
	    <tbody>
	        <?php foreach(papi_get_field('utilities_values') as $value): ?>
	        <tr>   
				<td><?php echo $value['utilities_system']; ?></td>
				<td><?php echo $value['utilities_description']; ?></td>
	        </tr>    
	        <?php endforeach; ?>
	    </tbody>
	</table>
</div>