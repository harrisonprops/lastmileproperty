<?php

class LastMileProperty_Page_Type extends Papi_Page_Type {
	public function getColors(){
		global $ps_colors;
		return $ps_colors;
	}
	public function meta() {
		return [
		  'name'        => 'Last Mile Property',
		  'description' => 'A page with the needed fields for the landing page',
		  'post_type'   => 'lmproperty',
		  'template'    => 'single-lastmileproperty.php'
		];
	}
	public function remove() {
		return ['editor', 'commentdiv', 'comments', 'thumbnail','authordiv','pageparentdiv'];
	}
	public function register() {
		
		//INTRO
		$this->box( 'Intro', [
			papi_property([
				'type'  => 'string',
				'slug'	=> 'label_title',
				'title' => 'Ribbon text'
			]),
			papi_property([
				'type'  => 'string',
				'slug'	=> 'intro_title',
				'title' => 'Title'
			]),
			papi_property([
			  'title'    => 'Text',
			  'slug'	 => 'intro_text',
			  'type'  	 => 'text',
			  'settings' => [
			    'allow_html' => true
			  ]
			])
		]);
		//DOWNLOAD SPEC SHEET
		$this->box( 'Download PDF', [
			papi_property([
				'title' => 'File',
				'slug'  => 'pdf_spec_sheet',
				'type'  => 'file'
			])
		]);
		//Distance
		$this->box( 'Distance Section', [
			papi_property([
				'type'  => 'string',
				'title' => 'Title',
				'slug'  => 'distance_title'
			]),
			papi_property([
				'type'  => 'string',
				'title' => 'Subtitle',
				'slug'  => 'distance_subtitle'
			]),
			papi_property([
				'title'    => 'Stats',
				'slug'     => 'distance_values',
				'type'     => 'repeater',
				'settings' => [
					'limit'  => 4,
					'add_new_label' => 'Add new stat',
					'layout' => 'row',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'Title',
							'slug'  => 'distance_value_title'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Cross Streets',
							'slug'  => 'distance_value_streets',
							'settings' => [
							    'allow_html' => true
							]
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'slug',
							'slug'  => 'distance_value_slug'
						]),
						papi_property([
							'title' => 'Percent',
							'slug'  => 'distance_value_number',
							'type'  => 'number',
							'settings' => [
								'step' => 1,
								'min' => 0,
								'max' => 100,
							]
						])
					]
				]
			]),
			papi_property([
				'type'  => 'string',
				'title' => 'Source',
				'slug'  => 'distance_source'
			])
		]);
		//Map
		$this->box( 'Map Section', [
			papi_property([
				'title' => 'Center Latitude',
				'slug'  => 'map_center_lat',
				'type'  => 'string'
			]),
			papi_property([
				'title' => 'Center Longitude',
				'slug'  => 'map_center_long',
				'type'  => 'string'
			]),
			papi_property([
				'title' => 'Map Zoom',
				'slug'  => 'map_zoom',
				'type'  => 'number'
			]),
			papi_property([
				'title'    => 'Map Markers',
				'slug'     => 'map_markers',
				'type'     => 'repeater',
				'settings' => [
					'add_new_label' => 'Add new marker',
					'items'  => [
						papi_property([
							'title' => 'Title',
							'slug'  => 'map_marker_label',
							'type'  => 'string'
						]),
						papi_property([
							'title' => 'Text',
							'slug'  => 'map_marker_text',
							'type'  => 'string'
						]),
						papi_property([
							'title' => 'Longitude',
							'slug'  => 'map_marker_longitude',
							'type'  => 'string'
						]),
						papi_property([
							'title' => 'Latitude',
							'slug'  => 'map_marker_latitude',
							'type'  => 'string'
						])
					]
				]
			]),
		]);
		//Time
		$this->box( 'Time Section', [
			papi_property([
				'type'  => 'string',
				'title' => 'Title',
				'slug'  => 'time_section_title',
				'settings' => [
				    'allow_html' => true
				]
			]),
			papi_property([
				'title'    => 'Stats',
				'slug'     => 'time_values',
				'type'     => 'repeater',
				'settings' => [
					'limit' => 4,
					'add_new_label' => 'Add new stat',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'Title',
							'slug'  => 'time_value_title'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Slug',
							'slug'  => 'time_value_slug'
						]),
						papi_property([
							'title' => 'Percent',
							'slug'  => 'time_value_number',
							'type'  => 'number',
							'settings' => [
								'step' => 1,
								'min' => 0,
								'max' => 100,
							]
						])
					]
				]
			]),
			papi_property([
				'type'  => 'string',
				'title' => 'Source',
				'slug'  => 'time_source'
			])
		]);
		//Stats
		/*
				TBD (probably should be static to avoid formating issues)
		*/
		//Age
		$this->box( 'Age Section', [
			papi_property([
				'type'  => 'string',
				'title' => 'Title',
				'slug'  => 'age_section_title'
			]),
			papi_property([
				'title'    => 'Stats',
				'slug'     => 'age_values',
				'type'     => 'repeater',
				'settings' => [
					'limit' => 3,
					'add_new_label' => 'Add new graph',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'Title',
							'slug'  => 'age_value_title'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Slug',
							'slug'  => 'age_value_slug'
						]),
						papi_property([
							'title' => 'Percent',
							'slug'  => 'age_value_number',
							'type'  => 'number',
							'settings' => [
								'step' => 1,
								'min' => 0,
								'max' => 100,
							]
						])
					]
				]
			]),
			papi_property([
				'type'  => 'string',
				'title' => 'Stat',
				'slug'  => 'age_summary'
			]),
			papi_property([
				'type'  => 'string',
				'title' => 'Source',
				'slug'  => 'age_source'
			])
		]);
		//CTA
		$this->box( 'Call to action', [
			papi_property([
				'type'  => 'string',
				'slug'	=> 'cta_title',
				'title' => 'Title'
			]),
			papi_property([
			  'title'    => 'Text',
			  'slug'	 => 'cta_text',
			  'type'  	 => 'text',
			  'settings' => [
			    'allow_html' => true
			  ]
			]),
			papi_property([
				'type'  => 'string',
				'slug'	=> 'cta_button',
				'title' => 'Button Label'
			]),
			papi_property( [
				'title'    => 'Button URL',
				'slug'     => 'cta_url',
				'type'     => 'cta_url'
			] )
		]);
		//Specs Tab
		$this->box( 'Site Specs tab', [
			papi_property( [
			  'title' => 'Site Spec Map',
			  'slug'  => 'sitespecs_map',
			  'type'  => 'image'
			]),
			papi_property([
				'title'    => 'Site Specs table rows',
				'slug'     => 'sitespecs_values',
				'type'     => 'repeater',
				'settings' => [
					'layout'=> 'row',
					'add_new_label' => 'Add new row',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'Building',
							'slug'  => 'spec_building'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Const Date',
							'slug'  => 'spec_constdate'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Type',
							'slug'  => 'spec_type'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Total SF',
							'slug'  => 'spec_totalsf'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'WHSE Office SF',
							'slug'  => 'spec_whseofficesf'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Column Clear',
							'slug'  => 'spec_columnclear'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Doc Spacing',
							'slug'  => 'spec_docspacing'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'FRZR/Position',
							'slug'  => 'spec_frzrposition'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Temp Refrig',
							'slug'  => 'spec_temprefrig'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Range',
							'slug'  => 'spec_range'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Tenant',
							'slug'  => 'spec_tenant'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'FRZR/Position',
							'slug'  => 'spec_totals_frzrposition'
						])
					]
				]
			]),
			papi_property([
				'title'    => 'Site Specs table totals',
				'slug'     => 'sitespecs_totals',
				'type'     => 'repeater',
				'settings' => [
					'layout'=> 'row',
					'add_new_label' => 'Add new row',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'Total SF',
							'slug'  => 'spec_totals_totalsf'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'WHSE Office SF',
							'slug'  => 'spec_totals_whseofficesf'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Column Clear',
							'slug'  => 'spec_totals_columnclear'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'FRZR/Position',
							'slug'  => 'spec_totals_frzrposition'
						])
					]
				]
			])
		]);
		//Utilities Tab
		$this->box( 'Utilities tab', [
			papi_property([
				'title'    => 'Utilities table rows',
				'slug'     => 'utilities_values',
				'type'     => 'repeater',
				'settings' => [
					'layout'=> 'row',
					'add_new_label' => 'Add new row',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'System',
							'slug'  => 'utilities_system'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Description',
							'slug'  => 'utilities_description'
						])
					]
				]
			])
		]);
		//About Tab
		$this->box( 'About tab', [
			papi_property([
				'title'    => 'About table rows',
				'slug'     => 'about_values',
				'type'     => 'repeater',
				'settings' => [
					'layout'=> 'row',
					'add_new_label' => 'Add new row',
					'items' => [
						papi_property([
							'type'  => 'string',
							'title' => 'Label',
							'slug'  => 'about_label'
						]),
						papi_property([
							'type'  => 'string',
							'title' => 'Value',
							'slug'  => 'about_value'
						]),
					]
				]
			])
		]);
	}

}