<?php
/*
Plugin Name: Harrison Properties - Last Mile
* Plugin URI: https://bitbucket.org/jasonjgeiger/lastmileproperty/
 * Description: This a Wordpress plugin for custom post-type infographic pages for Last Mile Properties. Check the README.MD for more details.
 * Author: Jason Geiger
 * Author URI: http://130public.net
 * Version: 1.0
*/

/*
PATH FOR ASSETS
*/
$HPLM_PATH = plugin_dir_url( __FILE__ );

/*
PAPI CHECK
The Papi plugin is required because it creates the custom fields and execution methods for the Last Mile Property posts.
*/
function hplm_check_required_plugins() {
    if ( current_user_can( 'activate_plugins' ) ) {
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if ( ! is_plugin_active( 'papi/papi-loader.php' ) ){
            add_action( 'admin_notices', function(){
	        	$url = 'https://wp-papi.github.io/';
			    echo '
			    <div class="error">
			        <p>The <a href="' . $url . '">The Papi Plugin</a> is required.</p>
			    </div>
			    ';	        
	        });
        }
    }
}
add_action( 'plugins_loaded', 'hplm_check_required_plugins' );

/*
SET POSTTYPE
Create a custom wordpress posttype to manage Last Mile Property posts separately
*/
function hplm_register_lastmile() {
    register_post_type( 'lmproperty',
		array(
			'labels' => array(
				'name' => __( 'Last Mile Properties' ),
				'singular_name' => __( 'Last Mile' )
			),
			'hierarchical' => false,
			'menu_position' => 5,
			'show_in_nav_menus' => true,
			'publicly_queryable' => true,
			'rewrite' => array( 'slug' => 'last-mile-properties' ),
			'public' => true,
			'has_archive' => false,
			'supports' => array( 'title','thumbnail','editor','page-attributes' ),
		)
	);
}
add_action( 'init', 'hplm_register_lastmile' );

/*
GRAB PAPI template Classes
*/
add_filter( 'papi/settings/directories', function (){
	return plugin_dir_path( __FILE__ ) . 'post-types';
});


/*
SET Last Mile Property posts
These are separate from the theme to reduce confusion and consolidate methods for Last Mile Properties	
*/
add_filter( 'single_template', function( $post_template ){
    if ( get_post_type() == 'lmproperty' ) {
        $post_template = dirname( __FILE__ ) . '/post-templates/single-lastmileproperty.php';
    }
    return $post_template;
});
/*
META TAGS
*/
function hplm_meta(){
	echo '<meta name="viewport" content="width=375, initial-scale=1.0">';
}
add_action( 'wp_head', 'hplm_meta' , 2 );
/*
INCLUDE CSS & JS
*/
function hplm_assets() {
	if ( get_post_type() != 'lmproperty' ) return false;
	wp_enqueue_style( 'themify-google-fonts-2', 'https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600','',false,'all');
	wp_enqueue_style( 'hplm_style', plugin_dir_url( __FILE__ ) . 'assets/style.css',
		array('theme-style','themify-media-queries','ultra-header','themify-customize','themify-google-fonts'),
	false,'all');
	
	wp_enqueue_script( 'hplm_script', plugin_dir_url( __FILE__ ) . 'assets/scripts-min.js', array ('jquery'),false, true);
	wp_enqueue_script('hplm_map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBLClFp6kiQnYQa_A6RScxTNcgPN0cHG-c&callback=hplm_mapping','',false,true);
}
add_action('wp_enqueue_scripts', 'hplm_assets', 80);