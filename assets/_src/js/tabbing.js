/*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*/
var HPLM = HPLM || {};
HPLM.tabbing = {
  tablist: document.querySelectorAll('[role="tablist"]')[0],
  tabs:"",
  panels:"",
  keys: {
    end: 35,
    home: 36,
    left: 37,
    up: 38,
    right: 39,
    down: 40,
    delete: 46
  },
  direction: {
    37: -1,
    38: -1,
    39: 1,
    40: 1
  },
  init: function(){
    HPLM.tabbing.delay =  0; //HPLM.tabbing.determineDelay();
    console.log("Init tabbing");
    HPLM.tabbing.generateArrays();
    for (i = 0; i < HPLM.tabbing.tabs.length; ++i) {
      HPLM.tabbing.addListeners(i);
    };
  },
  generateArrays: function() {
    HPLM.tabbing.tabs = document.querySelectorAll('[role="tab"]');
    HPLM.tabbing.panels = document.querySelectorAll('[role="tabpanel"]');
  },
   addListeners: function(index) {
    HPLM.tabbing.tabs[index].addEventListener('click', HPLM.tabbing.clickEventListener);
    HPLM.tabbing.tabs[index].addEventListener('keydown', HPLM.tabbing.keydownEventListener);
    HPLM.tabbing.tabs[index].addEventListener('keyup', HPLM.tabbing.keyupEventListener);

    // Build an array with all tabs (<button>s) in it
    HPLM.tabbing.tabs[index].index = index;
  },
  // When a tab is clicked, activateTab is fired to activate it
  clickEventListener: function(event) {
	//Scroll to top
    var stickPosition = document.getElementById("hplm-header-image").offsetHeight;
    window.scrollTo({
	    top: stickPosition,
	    behavior: "instant"
	});
	//
    var tab = event.target;
    HPLM.tabbing.activateTab(tab, false);
    
  },

  // Handle keydown on tabs
  keydownEventListener: function(event) {
    var key = event.keyCode;

    switch (key) {
      case keys.end:
        event.preventDefault();
        // Activate last tab
        HPLM.tabbing.activateTab(tabs[tabs.length - 1]);
        break;
      case keys.home:
        event.preventDefault();
        // Activate first tab
        HPLM.tabbing.activateTab(tabs[0]);
        break;

      // Up and down are in keydown
      // because we need to prevent page scroll >:)
      case keys.up:
      case keys.down:
        HPLM.tabbing.determineOrientation(event);
        break;
    };
  },

  // Handle keyup on tabs
  keyupEventListener: function(event) {
    var key = event.keyCode;

    switch (key) {
      case keys.left:
      case keys.right:
        HPLM.tabbing.determineOrientation(event);
        break;
      case keys.delete:
        HPLM.tabbing.determineDeletable(event);
        break;
    };
  },

  // When a tablist’s aria-orientation is set to vertical,
  // only up and down arrow should function.
  // In all other cases only left and right arrow function.
  determineOrientation:function(event) {
    var key = event.keyCode;
    var vertical = HPLM.tabbing.tablist.getAttribute('aria-orientation') == 'vertical';
    var proceed = false;

    if (vertical) {
      if (key === keys.up || key === keys.down) {
        event.preventDefault();
        proceed = true;
      };
    }
    else {
      if (key === keys.left || key === keys.right) {
        proceed = true;
      };
    };

    if (proceed) {
      HPLM.tabbing.switchTabOnArrowPress(event);
    };
  },

  // Either focus the next, previous, first, or last tab
  // depening on key pressed
  switchTabOnArrowPress: function(event) {
    var pressed = event.keyCode;

    for (x = 0; x < tabs.length; x++) {
      HPLM.tabbing.tabs[x].addEventListener('focus', HPLM.tabbing.focusEventHandler);
    };

    if (direction[pressed]) {
      var target = event.target;
      if (target.index !== undefined) {
        if (tabs[target.index + direction[pressed]]) {
          HPLM.tabbing.tabs[target.index + HPLM.tabbing.direction[pressed]].focus();
        }
        else if (pressed === HPLM.tabbing.keys.left || pressed === HPLM.tabbing.keys.up) {
          HPLM.tabbing.focusLastTab();
        }
        else if (pressed === HPLM.tabbing.keys.right || pressed == HPLM.tabbing.keys.down) {
          HPLM.tabbing.focusFirstTab();
        };
      };
    };
  },

  // Activates any given tab panel
  activateTab:function(tab, setFocus) {
    setFocus = setFocus || true;
    // Deactivate all other tabs
    HPLM.tabbing.deactivateTabs();

    // Remove tabindex attribute
    tab.removeAttribute('tabindex');

    // Set the tab as selected
    tab.setAttribute('aria-selected', 'true');

    // Get the value of aria-controls (which is an ID)
    var controls = tab.getAttribute('aria-controls');

    // Remove hidden attribute from tab panel to make it visible
    document.getElementById(controls).removeAttribute('hidden');

    // Set focus when required
    if (setFocus) {
      tab.focus();
    };

    //trigger event so we can do stuff when a tab is activated
    console.log('activate');
    window.dispatchEvent(new CustomEvent('tabActivated', { detail: { activeTab: tab } }))
  },

  // Deactivate all tabs and tab panels
  deactivateTabs: function() {
    for (t = 0; t < HPLM.tabbing.tabs.length; t++) {
      HPLM.tabbing.tabs[t].setAttribute('tabindex', '-1');
      HPLM.tabbing.tabs[t].setAttribute('aria-selected', 'false');
      HPLM.tabbing.tabs[t].removeEventListener('focus', HPLM.tabbing.focusEventHandler);
    };

    for (p = 0; p < HPLM.tabbing.panels.length; p++) {
      HPLM.tabbing.panels[p].setAttribute('hidden', 'hidden');
    };
  },

  // Make a guess
  focusFirstTab: function() {
    HPLM.tabbing.tabs[0].focus();
  },

  // Make a guess
  focusLastTab:function  () {
    HPLM.tabbing.tabs[HPLM.tabbing.tabs.length - 1].focus();
  },

  // Detect if a tab is deletable
  determineDeletable: function(event) {
    target = event.target;

    if (target.getAttribute('data-deletable') !== null) {
      // Delete target tab
      deleteTab(event, target);

      // Update arrays related to tabs widget
      HPLM.tabbing.generateArrays();

      // Activate the closest tab to the one that was just deleted
      if (target.index - 1 < 0) {
        HPLM.tabbing.activateTab(HPLM.tabbing.tabs[0]);
      }
      else {
        HPLM.tabbing.activateTab(HPLM.tabbing.tabs[target.index - 1]);
      };
    };
  },

  // Deletes a tab and its panel
  deleteTab: function(event) {
    var target = event.target;
    var panel = document.getElementById(target.getAttribute('aria-controls'));

    target.parentElement.removeChild(target);
    panel.parentElement.removeChild(panel);
  },

  // Determine whether there should be a delay
  // when user navigates with the arrow keys
  // determineDelay:function() {
  //   var hasDelay = directiontablist.hasAttribute('data-delay');
  //   var delay = 0;

  //   if (hasDelay) {
  //     var delayValue = HPLM.tabbing.tablist.getAttribute('data-delay');
  //     if (delayValue) {
  //       delay = delayValue;
  //     }
  //     else {
  //       // If no value is specified, default to 300ms
  //       delay = 300;
  //     };
  //   };

  //   return delay;
  // },

  //
  focusEventHandler: function(event) {
    var target = event.target;

    setTimeout(HPLM.tabbing.checkTabFocus, HPLM.tabbing.delay, target);
  },

  // Only activate tab on focus if it still has focus after the delay
  checkTabFocus:function(target) {
    focused = document.activeElement;

    if (target === focused) {
      HPLM.tabbing.activateTab(target, false);
    };
  }
};