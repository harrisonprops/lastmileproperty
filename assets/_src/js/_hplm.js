/**
LOAD NAMESPACE
**/
var HPLM = HPLM || {};
HPLM.util = {
	count:function(obj, intTo, intCur=0, speed=500){
		var countInt = setInterval(function(){
			if(intCur < intTo){
				intCur++;
			}else{
				intCur--;
			}
			obj.innerHTML = intCur;
			if(intTo == intCur) clearInterval(countInt);
		},speed);
	},
	sectionPosition: function(){
		HPLM.sections = [
			{name:"section-1",top:0},
			{name:"section-2",top:0},
			{name:"section-3",top:0},
			{name:"section-4",top:0},
			{name:"section-5",top:0}
		];
		for(var i=0; i<HPLM.sections.length; i++){
			HPLM.sections[i].top = document.getElementById(HPLM.sections[i].name).offsetTop;
		}
	},
	scrollTo:function(to, duration,element=undefined) {
		if(element == undefined) element = document.documentElement;
		if (duration <= 0) return;
		var difference = to - element.scrollTop;
		var perTick = difference / duration * 10;
		
		setTimeout(function() {
			element.scrollTop = element.scrollTop + perTick;
			if (element.scrollTop == to) return;
			scrollTo(element, to, duration - 10);
		}, 10);
	},
	animateSection:function(){
		var buffer = -400;
		for(var i=0; i<HPLM.sections.length; i++){
			var sectionObj = document.getElementById(HPLM.sections[i].name);
			if(HPLM.sections[i].top > window.scrollY && HPLM.sections[i].top < (window.scrollY+window.innerHeight+buffer)){
				sectionObj.classList.add("animate");
				if(HPLM.sections[i].name == "section-1" && HPLM.distance.autoAnimate != true){
					HPLM.distance.autoAnimate = true;
					HPLM.distance.setFocus(overview_vars.distance_defaut_tab);
					HPLM.distance.animateSilhouettes(overview_vars.distance_default);
					HPLM.util.count(HPLM.distance.percent,overview_vars.distance_default,0,10);
				}
				if(HPLM.sections[i].name == "section-3" && HPLM.time.autoAnimate != true){
					HPLM.time.autoAnimate = true;
					HPLM.time.setFocus(overview_vars.time_defaut_tab);
					HPLM.util.count(HPLM.time.clock,overview_vars.time_default,HPLM.time.clock.innerHTML,50);
				}
			}else{
				//sectionObj.classList.remove("animate");
			}
		}
	}
}
document.addEventListener('DOMContentLoaded', function() {
		console.log('I am ready. Are you?');
		window.scrollTop = 0;
		//LET'S INIT OR COMPONENTS & in order of appearance
		HPLM.tabbing.init();
		HPLM.nav.init();
		HPLM.distance.init();
		//MAPPING INITIATED IN THE FOOT VIA GOOGLE MAPS CALLBACK
		HPLM.time.init();
		HPLM.graphing.init();
		HPLM.util.sectionPosition();
		
		//Global actions are in HPLM.utils
		window.addEventListener('resize', function(){
			HPLM.util.sectionPosition();
		});
		window.addEventListener("scroll",function(){
			HPLM.nav.update();
			HPLM.util.animateSection();
	});
}, false);