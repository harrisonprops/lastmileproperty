var HPLM = HPLM || {};
HPLM.distance = {
  target:"",
  percent: document.getElementById("hplm-distance-percentage"),
  init: function(){
    console.log("Init HPLM.distance");
    this.controlButtons = document.querySelectorAll(".hplm-controls--distance button");
    for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].addEventListener("click",function(event){
        HPLM.distance.control(event);
        HPLM.distance.removeFocus();
        HPLM.distance.setFocus(event.target.id);
        event.preventDefault();
      });
    }
    this.buildSilhouettes();
  },
  setFocus:function(tab){
	  document.getElementById(tab).setAttribute('aria-selected', 'true');
  },
  removeFocus:function(){
	for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].setAttribute('tabindex', '-1');
      this.controlButtons[i].setAttribute('aria-selected', 'false');
    }
  },
  control:function(event){
    HPLM.distance.target = event.target.id;
    newPercentage = document.getElementById(HPLM.distance.target).dataset.value;
    HPLM.distance.percentage = newPercentage;
    //Increment percentage
    HPLM.util.count(HPLM.distance.percent,newPercentage,HPLM.distance.percent.innerHTML,10);
    //Animate people silhouettes
    HPLM.distance.animateSilhouettes(newPercentage);

  },
  buildSilhouettes:function(){
    var parent = document.querySelector('.hplm-silhouettes');
    for(i=1; i <= 52; i++){
      //var silhouette = document.createElement("div",{"class":"silhouette silhouette-'+i+'"});
      var silhouette = document.createElement("div");
      silhouette.setAttribute("class","silhouette silhouette-"+i);
      parent.appendChild(silhouette);
    }
  },
  animateSilhouettes:function(percentage){
    //13columns * 4 rows 52 = 100%
    var evenPercentage = Math.round(percentage / 2) *2; //nearest even
    var people = document.querySelectorAll(".silhouette");
    var numberOfPeople = evenPercentage/2;
    people.forEach(function(person,i) {
      //console.log(person.getAttribute('class'));
      if(i < numberOfPeople){
        person.classList.add('silhouette-animated');
      }else{
        person.classList.remove('silhouette-animated');
      }
    });
  }
}