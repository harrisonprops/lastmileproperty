var HPLM = HPLM || {};
HPLM.nav = {
  scrollPosition:window.scrollY,
  init:function(){
    console.log("Init HPLM.nav");
    this.stickPosition = this.getStickPosition();
    HPLM.nav.headerImage();
    window.addEventListener("scroll",function(){
        HPLM.nav.update();
    });
    window.addEventListener('tabActivated', function(eventData){
      HPLM.nav.headerImage(eventData);
      HPLM.nav.minimizeWelcome(eventData);
    });
  },
  getStickPosition: function(){
	  return (document.getElementById("hplm-header").offsetHeight);
  },
  headerImage:function(e=undefined){
    activeTabId = (e==undefined) ? "overview" : e.detail.activeTab.getAttribute("id");
    headerContainer = document.getElementById("hplm-header-image");
    console.log(activeTabId);
    switch(activeTabId){
      case "overview":
        headerContainer.className = "hplm-header-image hplm-header-image--overview";
        break;
      case "about":
        headerContainer.className = "hplm-header-image hplm-header-image--about";
        break;
      case "sitespecs":
        headerContainer.className = "hplm-header-image hplm-header-image--sitespecs";
        break;
      case "utilities":
        headerContainer.className = "hplm-header-image hplm-header-image--utilities";
        break;
      default:
        headerContainer.className = "hplm-header-image hplm-header-image--overview";
        break;

    }
  },
  minimizeWelcome:function(e){
    activeTabId = e.detail.activeTab.getAttribute("id");
    if(activeTabId == "overview"){
      document.getElementById("hplm-intro-welcome").classList.remove('hplm-minimize');
    }else{
      document.getElementById("hplm-intro-welcome").classList.add('hplm-minimize');
    }
  },
  update:function(){
	this.stickPosition= this.getStickPosition();
    this.scrollPosition = window.scrollY; //NEED browser scroll height
    //console.log(this.scrollPosition+'/'+this.stickPosition );
    if(this.scrollPosition > this.stickPosition ){
      //console.log('greater');
      document.getElementById("hplm-nav").classList.add('fixed');
      document.getElementById("hplm-nav").setAttribute('style','top:'+document.getElementById("headerwrap").offsetHeight+'px');
    }else{
      //console.log('lesser');
      document.getElementById("hplm-nav").classList.remove('fixed');
      document.getElementById("hplm-nav").removeAttribute('style');
    }
  }
}