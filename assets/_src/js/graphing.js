var HPLM = HPLM || {};
HPLM.graphing = {
  genxPie: document.querySelector(".hplm-graph-genx .pie"),
  boomersPie: document.querySelector(".hplm-graph-boomers .pie"),
  millenialsPie: document.querySelector(".hplm-graph-millenials .pie"),
  genxPercent: document.querySelector(".hplm-graph-genx .percent"),
  boomersPercent: document.querySelector(".hplm-graph-boomers .percent"),
  millenialsPercent: document.querySelector(".hplm-graph-millenials .percent"),
  init: function(){
    console.log("Let's graph something");
  },
  animate: function(){
    //Animate pie
    genxPie.classList.add("animate");
    boomersPie.classList.add("animate");
    millenialsPie.classList.add("animate");
    //Animate percentage count
    HPLM.util.count(genxPercent,genxPercent.dataset.value,1200);
    HPLM.util.count(boomersPercent,boomersPercent.dataset.value,1400);
    HPLM.util.count(millenialsPercent,millenialsPercent.dataset.value,1600);
  }
}