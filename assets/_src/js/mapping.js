/**
 * MAPPING
 * https://developers.google.com/maps/documentation/javascript/tutorial
 * MARKER
 * https://developers.google.com/maps/documentation/javascript/custom-markers
 * STYLING
 * https://developers.google.com/maps/documentation/javascript/styling
 */
var HPLM = HPLM || {};
HPLM.mapping = {
  init:function(){
    console.log('Init Mapping');
    var map = new google.maps.Map(document.getElementById('hplm-map'), {
      center: map_vars.center,
      zoom: map_vars.zoom,
      styles: [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#242f3e"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#746855"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#242f3e"
            }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d59563"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d59563"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#263c3f"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6b9a76"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#38414e"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#212a37"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9ca5b3"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#746855"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#1f2835"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#f3d19c"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2f3948"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d59563"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#17263c"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#515c6d"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#17263c"
            }
          ]
        }
      ]
    });
    //SET MARKERS
    map_vars.markers.forEach(function(feature) {
	  var infowindow = new google.maps.InfoWindow({
        content: "<p class='hplm-infowindow-p'><b>"+feature.label+"</b><br/>"+feature.text+"</p>"
      });
      var icon = {
		url: map_vars.pin,
		size: new google.maps.Size(40, 50),
		scaledSize: new google.maps.Size(40, 50)
	  };
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(feature.lat, feature.lng),
        icon: icon,
        map: map
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
    });
  }
};