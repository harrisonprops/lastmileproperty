var HPLM = HPLM || {};
HPLM.time = {
  target: "",
  clock: document.getElementById("hplm-clock-time"),
  init: function(){
    console.log("Init HPLM.time");
    this.controlButtons = document.querySelectorAll(".hplm-controls--time button")
    for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].addEventListener("click",function(event){
        HPLM.time.control(event);
        HPLM.time.removeFocus();
        HPLM.time.setFocus(event.target.id);
        event.preventDefault();
      });
    }
  },
  setFocus:function(tab){
	  document.getElementById(tab).setAttribute('aria-selected', 'true');
  },
  removeFocus:function(){
	for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].setAttribute('tabindex', '-1');
      this.controlButtons[i].setAttribute('aria-selected', 'false');
    }
  },
  control:function(event){
    HPLM.time.target = event.target.id;
    newTime = document.getElementById(HPLM.time.target).dataset.value;
    HPLM.util.count(HPLM.time.clock,newTime,HPLM.time.clock.innerHTML,50);
  }
}