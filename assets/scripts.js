/**
LOAD NAMESPACE
**/
var HPLM = HPLM || {};
HPLM.util = {
	count:function(obj, intTo, intCur=0, speed=500){
		var countInt = setInterval(function(){
			if(intCur < intTo){
				intCur++;
			}else{
				intCur--;
			}
			obj.innerHTML = intCur;
			if(intTo == intCur) clearInterval(countInt);
		},speed);
	},
	sectionPosition: function(){
		HPLM.sections = [
			{name:"section-1",top:0},
			{name:"section-2",top:0},
			{name:"section-3",top:0},
			{name:"section-4",top:0},
			{name:"section-5",top:0}
		];
		for(var i=0; i<HPLM.sections.length; i++){
			HPLM.sections[i].top = document.getElementById(HPLM.sections[i].name).offsetTop;
		}
	},
	scrollTo:function(to, duration,element=undefined) {
		if(element == undefined) element = document.documentElement;
		if (duration <= 0) return;
		var difference = to - element.scrollTop;
		var perTick = difference / duration * 10;
		
		setTimeout(function() {
			element.scrollTop = element.scrollTop + perTick;
			if (element.scrollTop == to) return;
			scrollTo(element, to, duration - 10);
		}, 10);
	},
	animateSection:function(){
		var buffer = -400;
		for(var i=0; i<HPLM.sections.length; i++){
			var sectionObj = document.getElementById(HPLM.sections[i].name);
			if(HPLM.sections[i].top > window.scrollY && HPLM.sections[i].top < (window.scrollY+window.innerHeight+buffer)){
				sectionObj.classList.add("animate");
				if(HPLM.sections[i].name == "section-1" && HPLM.distance.autoAnimate != true){
					HPLM.distance.autoAnimate = true;
					HPLM.distance.setFocus(overview_vars.distance_defaut_tab);
					HPLM.distance.animateSilhouettes(overview_vars.distance_default);
					HPLM.util.count(HPLM.distance.percent,overview_vars.distance_default,0,10);
				}
				if(HPLM.sections[i].name == "section-3" && HPLM.time.autoAnimate != true){
					HPLM.time.autoAnimate = true;
					HPLM.time.setFocus(overview_vars.time_defaut_tab);
					HPLM.util.count(HPLM.time.clock,overview_vars.time_default,HPLM.time.clock.innerHTML,50);
				}
			}else{
				//sectionObj.classList.remove("animate");
			}
		}
	}
}
document.addEventListener('DOMContentLoaded', function() {
		console.log('I am ready. Are you?');
		window.scrollTop = 0;
		//LET'S INIT OR COMPONENTS & in order of appearance
		HPLM.tabbing.init();
		HPLM.nav.init();
		HPLM.distance.init();
		//MAPPING INITIATED IN THE FOOT VIA GOOGLE MAPS CALLBACK
		HPLM.time.init();
		HPLM.graphing.init();
		HPLM.util.sectionPosition();
		
		//Global actions are in HPLM.utils
		window.addEventListener('resize', function(){
			HPLM.util.sectionPosition();
		});
		window.addEventListener("scroll",function(){
			HPLM.nav.update();
			HPLM.util.animateSection();
	});
}, false);
var HPLM = HPLM || {};
HPLM.distance = {
  target:"",
  percent: document.getElementById("hplm-distance-percentage"),
  init: function(){
    console.log("Init HPLM.distance");
    this.controlButtons = document.querySelectorAll(".hplm-controls--distance button");
    for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].addEventListener("click",function(event){
        HPLM.distance.control(event);
        HPLM.distance.removeFocus();
        HPLM.distance.setFocus(event.target.id);
        event.preventDefault();
      });
    }
    this.buildSilhouettes();
  },
  setFocus:function(tab){
	  document.getElementById(tab).setAttribute('aria-selected', 'true');
  },
  removeFocus:function(){
	for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].setAttribute('tabindex', '-1');
      this.controlButtons[i].setAttribute('aria-selected', 'false');
    }
  },
  control:function(event){
    HPLM.distance.target = event.target.id;
    newPercentage = document.getElementById(HPLM.distance.target).dataset.value;
    HPLM.distance.percentage = newPercentage;
    //Increment percentage
    HPLM.util.count(HPLM.distance.percent,newPercentage,HPLM.distance.percent.innerHTML,10);
    //Animate people silhouettes
    HPLM.distance.animateSilhouettes(newPercentage);

  },
  buildSilhouettes:function(){
    var parent = document.querySelector('.hplm-silhouettes');
    for(i=1; i <= 52; i++){
      //var silhouette = document.createElement("div",{"class":"silhouette silhouette-'+i+'"});
      var silhouette = document.createElement("div");
      silhouette.setAttribute("class","silhouette silhouette-"+i);
      parent.appendChild(silhouette);
    }
  },
  animateSilhouettes:function(percentage){
    //13columns * 4 rows 52 = 100%
    var evenPercentage = Math.round(percentage / 2) *2; //nearest even
    var people = document.querySelectorAll(".silhouette");
    var numberOfPeople = evenPercentage/2;
    people.forEach(function(person,i) {
      //console.log(person.getAttribute('class'));
      if(i < numberOfPeople){
        person.classList.add('silhouette-animated');
      }else{
        person.classList.remove('silhouette-animated');
      }
    });
  }
}
var HPLM = HPLM || {};
HPLM.graphing = {
  genxPie: document.querySelector(".hplm-graph-genx .pie"),
  boomersPie: document.querySelector(".hplm-graph-boomers .pie"),
  millenialsPie: document.querySelector(".hplm-graph-millenials .pie"),
  genxPercent: document.querySelector(".hplm-graph-genx .percent"),
  boomersPercent: document.querySelector(".hplm-graph-boomers .percent"),
  millenialsPercent: document.querySelector(".hplm-graph-millenials .percent"),
  init: function(){
    console.log("Let's graph something");
  },
  animate: function(){
    //Animate pie
    genxPie.classList.add("animate");
    boomersPie.classList.add("animate");
    millenialsPie.classList.add("animate");
    //Animate percentage count
    HPLM.util.count(genxPercent,genxPercent.dataset.value,1200);
    HPLM.util.count(boomersPercent,boomersPercent.dataset.value,1400);
    HPLM.util.count(millenialsPercent,millenialsPercent.dataset.value,1600);
  }
}
/**
 * MAPPING
 * https://developers.google.com/maps/documentation/javascript/tutorial
 * MARKER
 * https://developers.google.com/maps/documentation/javascript/custom-markers
 * STYLING
 * https://developers.google.com/maps/documentation/javascript/styling
 */
var HPLM = HPLM || {};
HPLM.mapping = {
  init:function(){
    console.log('Init Mapping');
    var map = new google.maps.Map(document.getElementById('hplm-map'), {
      center: map_vars.center,
      zoom: map_vars.zoom,
      styles: [
        {
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#242f3e"
            }
          ]
        },
        {
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#746855"
            }
          ]
        },
        {
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#242f3e"
            }
          ]
        },
        {
          "featureType": "administrative.locality",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d59563"
            }
          ]
        },
        {
          "featureType": "poi",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d59563"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#263c3f"
            }
          ]
        },
        {
          "featureType": "poi.park",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#6b9a76"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#38414e"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#212a37"
            }
          ]
        },
        {
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#9ca5b3"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#746855"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "geometry.stroke",
          "stylers": [
            {
              "color": "#1f2835"
            }
          ]
        },
        {
          "featureType": "road.highway",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#f3d19c"
            }
          ]
        },
        {
          "featureType": "transit",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#2f3948"
            }
          ]
        },
        {
          "featureType": "transit.station",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#d59563"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "geometry",
          "stylers": [
            {
              "color": "#17263c"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.fill",
          "stylers": [
            {
              "color": "#515c6d"
            }
          ]
        },
        {
          "featureType": "water",
          "elementType": "labels.text.stroke",
          "stylers": [
            {
              "color": "#17263c"
            }
          ]
        }
      ]
    });
    //SET MARKERS
    map_vars.markers.forEach(function(feature) {
	  var infowindow = new google.maps.InfoWindow({
        content: "<p class='hplm-infowindow-p'><b>"+feature.label+"</b><br/>"+feature.text+"</p>"
      });
      var icon = {
		url: map_vars.pin,
		size: new google.maps.Size(40, 50),
		scaledSize: new google.maps.Size(40, 50)
	  };
      var marker = new google.maps.Marker({
        position: new google.maps.LatLng(feature.lat, feature.lng),
        icon: icon,
        map: map
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
      });
    });
  }
};
var HPLM = HPLM || {};
HPLM.nav = {
  scrollPosition:window.scrollY,
  init:function(){
    console.log("Init HPLM.nav");
    this.stickPosition = this.getStickPosition();
    HPLM.nav.headerImage();
    window.addEventListener("scroll",function(){
        HPLM.nav.update();
    });
    window.addEventListener('tabActivated', function(eventData){
      HPLM.nav.headerImage(eventData);
      HPLM.nav.minimizeWelcome(eventData);
    });
  },
  getStickPosition: function(){
	  return (document.getElementById("hplm-header").offsetHeight);
  },
  headerImage:function(e=undefined){
    activeTabId = (e==undefined) ? "overview" : e.detail.activeTab.getAttribute("id");
    headerContainer = document.getElementById("hplm-header-image");
    console.log(activeTabId);
    switch(activeTabId){
      case "overview":
        headerContainer.className = "hplm-header-image hplm-header-image--overview";
        break;
      case "about":
        headerContainer.className = "hplm-header-image hplm-header-image--about";
        break;
      case "sitespecs":
        headerContainer.className = "hplm-header-image hplm-header-image--sitespecs";
        break;
      case "utilities":
        headerContainer.className = "hplm-header-image hplm-header-image--utilities";
        break;
      default:
        headerContainer.className = "hplm-header-image hplm-header-image--overview";
        break;

    }
  },
  minimizeWelcome:function(e){
    activeTabId = e.detail.activeTab.getAttribute("id");
    if(activeTabId == "overview"){
      document.getElementById("hplm-intro-welcome").classList.remove('hplm-minimize');
    }else{
      document.getElementById("hplm-intro-welcome").classList.add('hplm-minimize');
    }
  },
  update:function(){
	this.stickPosition= this.getStickPosition();
    this.scrollPosition = window.scrollY; //NEED browser scroll height
    //console.log(this.scrollPosition+'/'+this.stickPosition );
    if(this.scrollPosition > this.stickPosition ){
      //console.log('greater');
      document.getElementById("hplm-nav").classList.add('fixed');
      document.getElementById("hplm-nav").setAttribute('style','top:'+document.getElementById("headerwrap").offsetHeight+'px');
    }else{
      //console.log('lesser');
      document.getElementById("hplm-nav").classList.remove('fixed');
      document.getElementById("hplm-nav").removeAttribute('style');
    }
  }
}
/*
*   This content is licensed according to the W3C Software License at
*   https://www.w3.org/Consortium/Legal/2015/copyright-software-and-document
*/
var HPLM = HPLM || {};
HPLM.tabbing = {
  tablist: document.querySelectorAll('[role="tablist"]')[0],
  tabs:"",
  panels:"",
  keys: {
    end: 35,
    home: 36,
    left: 37,
    up: 38,
    right: 39,
    down: 40,
    delete: 46
  },
  direction: {
    37: -1,
    38: -1,
    39: 1,
    40: 1
  },
  init: function(){
    HPLM.tabbing.delay =  0; //HPLM.tabbing.determineDelay();
    console.log("Init tabbing");
    HPLM.tabbing.generateArrays();
    for (i = 0; i < HPLM.tabbing.tabs.length; ++i) {
      HPLM.tabbing.addListeners(i);
    };
  },
  generateArrays: function() {
    HPLM.tabbing.tabs = document.querySelectorAll('[role="tab"]');
    HPLM.tabbing.panels = document.querySelectorAll('[role="tabpanel"]');
  },
   addListeners: function(index) {
    HPLM.tabbing.tabs[index].addEventListener('click', HPLM.tabbing.clickEventListener);
    HPLM.tabbing.tabs[index].addEventListener('keydown', HPLM.tabbing.keydownEventListener);
    HPLM.tabbing.tabs[index].addEventListener('keyup', HPLM.tabbing.keyupEventListener);

    // Build an array with all tabs (<button>s) in it
    HPLM.tabbing.tabs[index].index = index;
  },
  // When a tab is clicked, activateTab is fired to activate it
  clickEventListener: function(event) {
	//Scroll to top
    var stickPosition = document.getElementById("hplm-header-image").offsetHeight;
    window.scrollTo({
	    top: stickPosition,
	    behavior: "instant"
	});
	//
    var tab = event.target;
    HPLM.tabbing.activateTab(tab, false);
    
  },

  // Handle keydown on tabs
  keydownEventListener: function(event) {
    var key = event.keyCode;

    switch (key) {
      case keys.end:
        event.preventDefault();
        // Activate last tab
        HPLM.tabbing.activateTab(tabs[tabs.length - 1]);
        break;
      case keys.home:
        event.preventDefault();
        // Activate first tab
        HPLM.tabbing.activateTab(tabs[0]);
        break;

      // Up and down are in keydown
      // because we need to prevent page scroll >:)
      case keys.up:
      case keys.down:
        HPLM.tabbing.determineOrientation(event);
        break;
    };
  },

  // Handle keyup on tabs
  keyupEventListener: function(event) {
    var key = event.keyCode;

    switch (key) {
      case keys.left:
      case keys.right:
        HPLM.tabbing.determineOrientation(event);
        break;
      case keys.delete:
        HPLM.tabbing.determineDeletable(event);
        break;
    };
  },

  // When a tablist’s aria-orientation is set to vertical,
  // only up and down arrow should function.
  // In all other cases only left and right arrow function.
  determineOrientation:function(event) {
    var key = event.keyCode;
    var vertical = HPLM.tabbing.tablist.getAttribute('aria-orientation') == 'vertical';
    var proceed = false;

    if (vertical) {
      if (key === keys.up || key === keys.down) {
        event.preventDefault();
        proceed = true;
      };
    }
    else {
      if (key === keys.left || key === keys.right) {
        proceed = true;
      };
    };

    if (proceed) {
      HPLM.tabbing.switchTabOnArrowPress(event);
    };
  },

  // Either focus the next, previous, first, or last tab
  // depening on key pressed
  switchTabOnArrowPress: function(event) {
    var pressed = event.keyCode;

    for (x = 0; x < tabs.length; x++) {
      HPLM.tabbing.tabs[x].addEventListener('focus', HPLM.tabbing.focusEventHandler);
    };

    if (direction[pressed]) {
      var target = event.target;
      if (target.index !== undefined) {
        if (tabs[target.index + direction[pressed]]) {
          HPLM.tabbing.tabs[target.index + HPLM.tabbing.direction[pressed]].focus();
        }
        else if (pressed === HPLM.tabbing.keys.left || pressed === HPLM.tabbing.keys.up) {
          HPLM.tabbing.focusLastTab();
        }
        else if (pressed === HPLM.tabbing.keys.right || pressed == HPLM.tabbing.keys.down) {
          HPLM.tabbing.focusFirstTab();
        };
      };
    };
  },

  // Activates any given tab panel
  activateTab:function(tab, setFocus) {
    setFocus = setFocus || true;
    // Deactivate all other tabs
    HPLM.tabbing.deactivateTabs();

    // Remove tabindex attribute
    tab.removeAttribute('tabindex');

    // Set the tab as selected
    tab.setAttribute('aria-selected', 'true');

    // Get the value of aria-controls (which is an ID)
    var controls = tab.getAttribute('aria-controls');

    // Remove hidden attribute from tab panel to make it visible
    document.getElementById(controls).removeAttribute('hidden');

    // Set focus when required
    if (setFocus) {
      tab.focus();
    };

    //trigger event so we can do stuff when a tab is activated
    console.log('activate');
    window.dispatchEvent(new CustomEvent('tabActivated', { detail: { activeTab: tab } }))
  },

  // Deactivate all tabs and tab panels
  deactivateTabs: function() {
    for (t = 0; t < HPLM.tabbing.tabs.length; t++) {
      HPLM.tabbing.tabs[t].setAttribute('tabindex', '-1');
      HPLM.tabbing.tabs[t].setAttribute('aria-selected', 'false');
      HPLM.tabbing.tabs[t].removeEventListener('focus', HPLM.tabbing.focusEventHandler);
    };

    for (p = 0; p < HPLM.tabbing.panels.length; p++) {
      HPLM.tabbing.panels[p].setAttribute('hidden', 'hidden');
    };
  },

  // Make a guess
  focusFirstTab: function() {
    HPLM.tabbing.tabs[0].focus();
  },

  // Make a guess
  focusLastTab:function  () {
    HPLM.tabbing.tabs[HPLM.tabbing.tabs.length - 1].focus();
  },

  // Detect if a tab is deletable
  determineDeletable: function(event) {
    target = event.target;

    if (target.getAttribute('data-deletable') !== null) {
      // Delete target tab
      deleteTab(event, target);

      // Update arrays related to tabs widget
      HPLM.tabbing.generateArrays();

      // Activate the closest tab to the one that was just deleted
      if (target.index - 1 < 0) {
        HPLM.tabbing.activateTab(HPLM.tabbing.tabs[0]);
      }
      else {
        HPLM.tabbing.activateTab(HPLM.tabbing.tabs[target.index - 1]);
      };
    };
  },

  // Deletes a tab and its panel
  deleteTab: function(event) {
    var target = event.target;
    var panel = document.getElementById(target.getAttribute('aria-controls'));

    target.parentElement.removeChild(target);
    panel.parentElement.removeChild(panel);
  },

  // Determine whether there should be a delay
  // when user navigates with the arrow keys
  // determineDelay:function() {
  //   var hasDelay = directiontablist.hasAttribute('data-delay');
  //   var delay = 0;

  //   if (hasDelay) {
  //     var delayValue = HPLM.tabbing.tablist.getAttribute('data-delay');
  //     if (delayValue) {
  //       delay = delayValue;
  //     }
  //     else {
  //       // If no value is specified, default to 300ms
  //       delay = 300;
  //     };
  //   };

  //   return delay;
  // },

  //
  focusEventHandler: function(event) {
    var target = event.target;

    setTimeout(HPLM.tabbing.checkTabFocus, HPLM.tabbing.delay, target);
  },

  // Only activate tab on focus if it still has focus after the delay
  checkTabFocus:function(target) {
    focused = document.activeElement;

    if (target === focused) {
      HPLM.tabbing.activateTab(target, false);
    };
  }
};
var HPLM = HPLM || {};
HPLM.time = {
  target: "",
  clock: document.getElementById("hplm-clock-time"),
  init: function(){
    console.log("Init HPLM.time");
    this.controlButtons = document.querySelectorAll(".hplm-controls--time button")
    for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].addEventListener("click",function(event){
        HPLM.time.control(event);
        HPLM.time.removeFocus();
        HPLM.time.setFocus(event.target.id);
        event.preventDefault();
      });
    }
  },
  setFocus:function(tab){
	  document.getElementById(tab).setAttribute('aria-selected', 'true');
  },
  removeFocus:function(){
	for (var i = 0; i < this.controlButtons.length; i++) {
      this.controlButtons[i].setAttribute('tabindex', '-1');
      this.controlButtons[i].setAttribute('aria-selected', 'false');
    }
  },
  control:function(event){
    HPLM.time.target = event.target.id;
    newTime = document.getElementById(HPLM.time.target).dataset.value;
    HPLM.util.count(HPLM.time.clock,newTime,HPLM.time.clock.innerHTML,50);
  }
}