var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var minify = require('gulp-minify');
 
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "./",
        port: 8080,
        open: true,
        notify: false
    });
    gulp.watch("./assets/_src/js/**/*.js", ['scripts'])
    gulp.watch("./assets/_src/sass/**/*.scss", ['sass'])
    gulp.watch("./*.html").on('change', browserSync.reload);
});
 
gulp.task('sass', function() {
    return gulp.src("./assets/_src/sass/style.scss")
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest("./assets"))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function() {
    return gulp.src('./assets/_src/js/*.js')
      .pipe(concat('scripts.js'))
      .pipe(minify())
      .pipe(gulp.dest('./assets'))
      .pipe(browserSync.stream());
  });

gulp.task('default', ['serve']);
 
