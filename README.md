# Intro
*This a Wordpress plugin for custom post-type infographic pages leveraging [PAPI](https://wp-papi.github.io/) for data structuring and storage. Outside of the dependencies noted below this plugin is self contained.*

## Dependencies
* [PAPI](https://wp-papi.github.io/)
* Google Fonts included through the Themeify theme
* HP Google Maps API key

## Implementation
* This plugin defines a custom posttype "lmproperty" found in `post-types/lastmileproperty.php`
* Data within each post is integrated through the PAPI plugin and the data structure is set in `post-templates/single-lastmileproperty.php`
* Styling is implemented using SCSS via `gulp serve`
* Interactivity is vanilla javascript (no library dependencies) and compiled using `gulp serve`
* Customizable components and component fields are noted below

## Components & their tasks

### Data structure & storage
Data is stored using PAPI and the data model can be found here `post-types/lastmileproperty.php`. All of the sections except the stats section are customizable (Varying levels of customization).

* Data entry
* Add data hooks to layout
* Build data model

### Intro & Header
*Header image and intro copy block*

### Distance
*Modify infographic state with toggles & data managed by CMS*


### Mapping
*Mapping on the infographic is implemented using Google Maps API and uses the API key already integrated into Harrisonprops.com. Map points Long/Lat are managed by CMS*

### Stats
*Based on dynamic layout this content is not editable in the CMS*

### Graphing
*Animated graphics with data managed by CMS*

### Tables
*Tables for About, Specs, Utilities. Table data managed by CMS*

### CTA
*Dynamic CTA messaging. Managed by CMS.*